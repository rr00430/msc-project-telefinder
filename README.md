# MSC PROJECT

Currently designed to run off of google colab using google drive.

This is just the code, the file structure needs to be manually setup.

## File Structure

```
Parent Folder/
│
├── dataset/                           # Contains all generated training segments
│   ├── images/                        # Directory for images
│   │   ├── train/                     # Training images
│   │   └── val/                       # Validation images
│   │
│   ├── labels/                        # Directory for labels
│   │   ├── train/                     # Training labels
│   │   └── val/                       # Validation labels
│   │
│   └── dataset.yaml                   # Configuration file for the dataset
│
├── Outputs/                           # Directory for storing output files
│
├── Pointclouds/                       # Directory for raw point cloud data
│
├── Processed images/                  # Directory for processed images with various encodings
│   ├── Height Difference Encoded/     # Height Difference Encoded images
│   ├── Height Encoded/                # Height Encoded images
│   └── Point Count Encoded/           # Point Count Encoded images
│
├── LidarEncoder.ipynb                 # Jupyter notebook for Lidar encoding
│
├── TeleFinder Presentation.ipynb      # Jupyter notebook for combining the other notebooks
│
├── TeleFinder.ipynb                   # Jupyter notebook for handling models
│
└── TrainingDataGen.ipynb              # Jupyter notebook for generating training segments
```
